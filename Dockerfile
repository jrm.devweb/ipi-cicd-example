FROM node:18.14.0-alpine

WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn --frozen-lockfile --production

COPY dist dist

CMD ["yarn", "prod"]
